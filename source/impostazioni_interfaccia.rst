.. _impostazioni_interfaccia:

#############################
IMPOSTAZIONI MENÙ INTERFACCIA
#############################
Il menù delle :guilabel:`IMPOSTAZIONI APPLICAZIONE > Interfaccia` si suddivide in finestre che, a loro volta, contengono ulteriori parametri.
Di seguito descrizione dei parametri.

******************
PROPRIETÀ GENERALI
******************
**Disabilita Salvataggio Layout**

   *Inserire descrizione*

**Marcatura Veloce**

   *Inserire descrizione*

**Moltiplicatore Copie**

   *Inserire descrizione*

**Ritardo iniziale laser [s]**

   *Inserire descrizione*

**Contatori e Foglio dati**

   *Inserire descrizione*

**Assicura valori unici nei contatori**

   *Inserire descrizione*

**Vista globale 3D**

   *Inserire descrizione*

**Reset Layout**

   *Inserire descrizione*

**Report Contatori e Posizione**

   *Inserire descrizione*

**Beep dopo marcatura**

   *Inserire descrizione*

**Salva dopo marcatura**

   *Inserire descrizione*

**Sovrascrivi con livello di progetto**

   *Inserire descrizione*

**Priorità ai livelli di progetto**

   *Inserire descrizione*

**Copie per piano**

   *Inserire descrizione*

**Abilita lotto e quantità**

   *Inserire descrizione*

**Work In Pause Enabled**

   *Inserire descrizione*

**Sposta Z con lavoro in pausa**

   *Inserire descrizione*

**Converte output in path**

   *Inserire descrizione*

**SVG con "rectmark"**

   *Inserire descrizione*

**Nasconde linee grigie con Rosso**

   *Inserire descrizione*

**Mostra Finestra Centratura Rosso**

   *Inserire descrizione*

*********
ACCESSORI
*********
**Apri/Chiudi Protezioni**

   *Inserire descrizione*

**Chiudi Protezioni allo start**

   *Inserire descrizione*

**Accessori Esterni**

   *Inserire descrizione*

**Traino Lastra TL Plus**

   *Inserire descrizione*

**Traino Zip**

   *Inserire descrizione*

**Mandrino con bascula**

   *Inserire descrizione*

**Disabilita centra partizione**

   *Inserire descrizione*

**Controllo Assi 3D**

   *Inserire descrizione*

**Posizioni multiple**

   *Inserire descrizione*

**Attendi le sicurezza prima di muovere gli assi**

   *Inserire descrizione*

**Usa comandi macchina Kinema**

   *Inserire descrizione*

**Assi opzionali XY**

   *Inserire descrizione*

**Carica targhette**

   *Inserire descrizione*

**Tavola rotante**

   *Inserire descrizione*

**Tavola rotante con lavoro AB**

   *Inserire descrizione*

**Tavola rotante disabilita check autorotor (Sarty)**

   *Inserire descrizione*

**Luci esterne**

   *Inserire descrizione*

**Verifica presenza mandrino**

   *Inserire descrizione*

**Magazzino semplice**

   *Inserire descrizione*

**Magazzino RC**

   *Inserire descrizione*

**Magazzino RC Luce Rossa**

   *Inserire descrizione*

**Magazzino RC allarmi extra**

   *Inserire descrizione*

**************
ACCESSORI ARIA
**************
**Pinze**

   *Inserire descrizione*

**Controllo Aria**

   *Inserire descrizione*

**Aria durante la Marcatura**

   *Inserire descrizione*

**Ritardo Aria durante la marcatura [0.1 s]**

   *Inserire descrizione*

**Abilita cassetto pneumatico**

   *Inserire descrizione*

**Abilita Porta Pneumatica Bassa**

   *Inserire descrizione*

**Abilita Bascula Mandrino**

   *Inserire descrizione*

**Caricatore Targhette Pneumatico su LWSA**

   *Inserire descrizione*

**Caricatore Targhette Pneumatico**

   *Inserire descrizione*

**Base Magnetica**

   *Inserire descrizione*

*******
VISIONE
*******
**Disabilita Anteprima Rosso**

   *Inserire descrizione*

**Abilita Webcam**

   *Inserire descrizione*

**Camera Assiale**

   *Inserire descrizione*

**Pattern Matching**

   *Inserire descrizione*

**Pattern Matching: Verifica con nessun modello trovato**

   *Inserire descrizione*

**Messa a fuoco automatica**

   *Inserire descrizione*

**Range di messa a fuoco [mm]**

   *Inserire descrizione*

**Risoluzione dinamica per CSV 0.0**

   *Inserire descrizione*

**Ritardo acquisizione Frame con Live [ms]**

   *Inserire descrizione*

**************************
VALIDAZIONE CODICE 1D E 2D
**************************
**Abilita lettore esterno**

   *Inserire descrizione*

**Porta seriale**

   *Inserire descrizione*

***************************
PROPRIETÀ GESTIONE PROGETTI
***************************
**Non aprire gli ultimi progetti**

   *Inserire descrizione*

**Salvataggio automatico**

   *Inserire descrizione*

**Chiusura automatica senza salvare**

   *Inserire descrizione*

**Apri un progetto per volta**

   *Inserire descrizione*

**Non chiedere di salvare i progetti**

   *Inserire descrizione*

**Read Project Code**

   *Inserire descrizione*

**Anteprima Rosso all'apertura del progetto**

   *Inserire descrizione*

***************
PROPRIETÀ DEBUG
***************
**Strumenti di Debug**

   *Inserire descrizione*

**Nascondi messaggio di stato**

   *Inserire descrizione*

**Comando stop dopo marcatura**

   *Inserire descrizione*

**Livelli Ottimizzati**

   *Inserire descrizione*

**Velocità di salto ottimizzata**

   *Inserire descrizione*

**Maschera ingressi (0=nasconde)**

   *Inserire descrizione*

**Chiusura rapida**

   *Inserire descrizione*

**Laser On Off Tester**

   *Inserire descrizione*

**Disabilita ROSSO da tasto F3**

   *Inserire descrizione*

**Disabilita STOP da tasto F4**

   *Inserire descrizione*
