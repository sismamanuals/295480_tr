﻿.. _barcode:

########################
BARCODE (CODICE A BARRE)
########################
Il codice a barre è un codice di identificazione costituito da un insieme di elementi grafici a contrasto elevato destinati alla lettura per mezzo di un sensore a scansione e decodificati per restituire l'informazione in essi contenuta.

A seconda del tipo di codice adottato vi sono dei limiti nel ridimensionamento, nel numero e nel tipo caratteri rappresentabili.

************************
CREAZIONE CODICE A BARRE
************************
L'attivazione si effettua selezionando la voce :guilabel:`Aggiungi Barcode` nella barra degli strumenti presenti nell'area di lavoro. 

.. _barcode_aggiungi_barcode:
.. figure:: _static/barcode_aggiungi_barcode.png
   :scale: 70 %
   :align: center

   Selezione *Aggiungi Barcode*

A questo punto apparirà sull'area di lavoro un codice standard, che si dovrà personalizzare nella finestra :guilabel:`Proprietà`.

Prima di eseguire la marcatura è necessario compilare le impostazioni descritte.

*****************
PROPRIETÀ BARCODE
*****************
Contiene le informazioni relative ai dati contenuti dal codice.

**Tipo**

   Seleziona il tipo di codice a barre che si desidera usare.

**Data**

   Dati contenuti nel codice a barre. Verificare la tipologia ed il numero di caratteri massimi che si possono includere.

**Colore**

   Definisce il colore del parametro laser che verrà usato per marcare il Barcode.

**Sfondo**

   Permette di inserire uno sfondo al Barcode. Si può inserire solo uno sfondo, che verrà marcato prima o dopo a seconda della posizione, nella finestra Livelli, dei colori scelti per matrix e sfondo.

**Negativo**

   Inverte il colore dello sfondo con il Barcode e viceversa.

**Testo visibile**

   Inserisce sotto al codice il testo in chiaro dei dati contenuti nel codice.

**Bordo**
   Inserisce un bordo attorno al Barcode utile a migliorare la leggibilità in caso di presenza di uno sfondo.

PROPRIETÀ BARCODE - ESPERTO
===========================
Contiene ulteriori informazioni relative ai dati contenuti dal codice.

**Sequenza Escape**

   *Inserire descrizione*

**Testo sopra**

   *Inserire descrizione*

**Font**

   Seleziona il font utilizzato.

**Dimensione font**

   Seleziona la dimensione del font utilizzato.

**Margine Testo**

   *Inserire descrizione*

**Bordo sopra**

   *Inserire descrizione*

**Bordo sotto**

   *Inserire descrizione*

**Bordo sinistro**

   *Inserire descrizione*

**Bordo destro**

   *Inserire descrizione*

**********************
PROPRIETÀ FILE GRAFICO
**********************
Contiene le informazioni relative alla dimensione e alla posizione del codice a barre nell'area di lavoro.

**Nome**

   *Inserire descrizione*

**Esterno**

   *Inserire descrizione*

**X** e **Y**

   Indica la posizione all'interno dell'area di lavoro del Barcode.

**Larghezza** e **Altezza**

   Definisce le dimensioni, in mm, del Barcode.

**Rotazione**

   Serve per introdurre un angolo di rotazione al codice a barre.

**Scala X** e **Scala Y**

   *Inserire descrizione*

**Spessore**

   *Inserire descrizione*

************************
MARCATURA CODICE A BARRE
************************

* Settare l'altezza Z[mm] del punto di marcatura nella finestra :guilabel:`Struttura Progetto` e premere :kbd:`GO`.
* Selezionare i parametri di marcatura nella finestra :guilabel:`Livelli`.
* Premere :kbd:`ROSSO` per centrare l'area di marcatura sul pezzo.
* Chiudere lo sportello.
* Premere :kbd:`START` per eseguire la marcatura.
