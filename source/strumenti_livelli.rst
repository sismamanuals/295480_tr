﻿#################
STRUMENTI LIVELLI
#################
Nel menù a tendina :guilabel:`Strumenti > Livelli` troviamo la finestra :guilabel:`LIVELLI`.
In essa ci sono due voci: Libreria e Livelli.
Ognuna di queste voci permette di settare le proprietà dei Livelli che contengono i parametri laser di marcatura.
Passeremo ora a descrivere ognuna delle due voci.

********
LIBRERIA
********

.. _strumenti_livelli_impostazioni_libreria:
.. figure:: _static/strumenti_livelli_impostazioni_libreria.png
   :width: 14 cm
   :align: center

   Menù impostazioni *Libreria*

Nella prima parte viene mostrata la macchina selezionata, il tipo di laser, l'indirizzo IP della macchina e il file Livelli attualmente usato.
In questa finestra è possibile selezionare/cambiare la macchina da utilizzare nel caso in cui il computer sia abilitato alla gestione di laser diverse.
Subito sotto è possibile vedere la cartella che contiene tutti i File Livello salvati nella macchina ed agire su di essi.
Selezionando in File Livello si possono operare le seguenti attività:

* :kbd:`Salva con nome` (permette di salvare una copia del livello selezionato e assegnargli un nome diverso);
* :kbd:`Elimina` (cancellare definitivamente il set di parametri selezionato);
* :kbd:`Importa` (permette di importare un File Livello esistente nella cartella Livelli del Laser);
* :kbd:`Esporta` (permette di esportare il set parametri selezionato e salvarlo in un altra cartella del PC e di rete).

*******
LIVELLI
*******

.. _strumenti_livelli_impostazioni_livelli:
.. figure:: _static/strumenti_livelli_impostazioni_livelli.png
   :width: 14 cm
   :align: center

   Menù impostazioni *Livelli*

Questa finestra mostra affiancati i livelli del set parametri selezionato e permette la loro comparazione, compilazione e/o modifica.
